FROM python:3.8

RUN pip install \
        docopt pathlib xnatbidsfns xnat \
    && \
    rm -r ${HOME}/.cache/pip

RUN apt-get update && apt-get install zip -y && apt-get install unzip && apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y

#Path a directory to store scripts
WORKDIR /usr/local/bin

COPY download-bids.sh /usr/local/bin
COPY create_local_archive.sh /usr/local/bin
COPY xnat2bids_local.py /usr/local/bin

LABEL org.nrg.commands="[{\"name\": \"download-bids\", \"description\": \"download-bids setup command. Securely downloads XNAT session(s) with BIDS and NIFTI resources into a user specified local directory in BIDS format.\", \"version\": \"1.1\", \"type\": \"docker-setup\", \"image\": \"radiologicsit/download-bids-setup:1.1\", \"command-line\": \"download-bids.sh -o host -i projectid -c /input -b /output\"}]"
