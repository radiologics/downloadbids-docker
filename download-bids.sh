#!/bin/bash

usage()
{
cat << EOF

This script securely downloads NIFTI and JSON resources from a user specified
XNAT project, and reorganises the files in a BIDS compliant directory
structure within a user specified output folder.

USAGE:
$0 [OPTIONS] [-o XNAT host URL] [-i project ID] [-c absolute path to local dir for temporary caching of XNAT exported files] [-b absolute path to output BIDS dir]

OPTIONS:
[-s session labels comma separated]
[-u username]
[-p password]
NOTE:
If you don't enter username and password as arguments to the script, you will be prompted to enter them interactively.
To see this message, enter $0 -h

EOF

}

while getopts i:o:c:b:u:p:s:xh opt
do
    case "$opt" in
        o)  host="$OPTARG";;
        i)  projectid="$OPTARG";;
        c)  base="$OPTARG";;
        b)  bidsdir="$OPTARG";;
        p)  pass="$OPTARG";;
        u)  uname="$OPTARG";;
        s)  sessions="$OPTARG";;
        h)  usage
            exit;;
        ?)  # Unknown flag
            usage
            exit 1;;
    esac
done

shift `expr $OPTIND - 1`

if [[ $OPTIND -le 1 ]]; then #no arguments
    usage
    exit 1
fi

if [[ -z $host ]]; then
    usage
    echo -e "\nPlease specify your XNAT host URL to -o flag.\n"
    exit 1
fi

if [[ -z $projectid ]]; then
    usage
    echo -e "\nPlease specify a XNAT project ID to the -p flag.\n"
    exit 1
fi

if [[ -z $base ]]; then
    usage
    echo -e "\nPlease specify a directory path for temporary caching of XNAT exported files using the -c flag.\n"
    exit 1
fi

if [[ -z $bidsdir ]]; then
    usage
    echo -e "\nPlease specify an output BIDS directory path to the -b flag.\n"
    exit 1
fi

#get username and password if not given in argument list
if [[ -z $uname ]]; then
    echo -n "XNAT Username: "
    read uname
fi
if [[ -z $pass ]]; then
    stty_orig=`stty -g`
    stty -echo
    echo -n "XNAT Password: "
    read -s pass
    stty $stty_orig
fi

export host projectid base bidsdir pass uname sessions

# This command securely downloads all NIFTI resources from the user specified project to the user specified
# local directory while preserving the same directory structure as XNAT’s internal archive.
echo -e "\nExporting NIFTI files from project: $projectid on $host\n"
/bin/bash create_local_archive.sh -f 'NIFTI'
# This command securely downloads required JSON resources from the user specified project to the user specified
# local directory while preserving the same directory structure as XNAT’s internal archive
echo -e "\nExporting JSON files from project: $projectid on $host\n"
/bin/bash create_local_archive.sh -x -f 'BIDS'

echo -e "\nCompleted creation of temporary local archive at $base\n"
echo -e "\nReorganizing the NIFTI & JSON files into a BIDS compliant structure\n"

# This command reorganizes the NIFTI and JSON files into a valid, BIDS compliant format in the user specified destination directory.
python3 xnat2bids_local.py $base $bidsdir

echo -e "\nCopying XNAT_metadata csv file from $base to $bidsdir and adding relevent .bidsignore file\n"

cp -R $base/XNAT_metadata $bidsdir
echo -e "/XNAT_metadata/**" > $bidsdir/.bidsignore


echo -e "\nDone!!!\n"

echo -e "\nCompleted BIDS directory is at $bidsdir\n"

echo -e "\nYou may remove the temporary archive at $base, if so desired\n"
